import unittest
import json
import requests 
import configparser
import time
from datetime import datetime

class TestApi(unittest.TestCase):
    """Integration Tests to chceck API"""

    @classmethod
    def setUpClass(TestApi):
        config = configparser.RawConfigParser()
        config.read('backend/configuration.properties')
        TestApi.username = config.get('UserSection', 'user.name')
        TestApi.password = config.get('UserSection', 'user.pass')
        TestApi.url = "{}://{}:{}/".format(
            config.get('WebSection', 'websocket.protocol'),
            config.get('WebSection', 'websocket.address'),
            config.get('WebSection', 'websocket.port'))


    def test_connection(self):
        url = TestApi.url
        self.assertEqual(200, requests.get(url).status_code)

    def test_unauthorized_access(self):
        url = TestApi.url + "api/v1.0/velocity"
        r = requests.get(url)
        self.assertEqual(401, r.status_code)
    
    def test_velocity_get(self):
        url = TestApi.url + "api/v1.0/velocity"
        r = requests.get(url, auth=(TestApi.username, TestApi.password))
        self.assertEqual(200, r.status_code)
        
    def test_velocity_write_read(self):
        url = TestApi.url + "api/v1.0/velocity"
        value = 23.32
        r = requests.post(url, json={"value": value}, auth=(TestApi.username, TestApi.password))
        self.assertEqual(201, r.status_code)
        r = requests.get(url, auth=(TestApi.username, TestApi.password))
        self.assertEqual(value, r.json()[0]['value'])

    def test_velocity_get_limit(self):
        url = TestApi.url + "api/v1.0/velocity"
        value1 = 11.21
        value2 = 22.32
        value3 = 33.43
        requests.post(url, json={"value": value1}, auth=(TestApi.username, TestApi.password))
        requests.post(url, json={"value": value2}, auth=(TestApi.username, TestApi.password))
        requests.post(url, json={"value": value3}, auth=(TestApi.username, TestApi.password))
        url = url + "?length=2"
        r = requests.get(url, auth=(TestApi.username, TestApi.password))
        self.assertEqual(2, len(r.json()))

    def test_distance_front_get(self):
        url = TestApi.url + "api/v1.0/distance/front"
        r = requests.get(url, auth=(TestApi.username, TestApi.password))
        self.assertEqual(200, r.status_code)
        self.assertIsNotNone(r.json())
    
    def test_distance_front_write_read(self):
        url = TestApi.url + "api/v1.0/distance/front"
        value = 1.345
        r = requests.post(url, json={"value": value}, auth=(TestApi.username, TestApi.password))
        self.assertEqual(201, r.status_code)
        r = requests.get(url, auth=(TestApi.username, TestApi.password))
        self.assertEqual(value, r.json()[0]['value'])

    def test_distance_back_get(self):
        url = TestApi.url + "api/v1.0/distance/back"
        r = requests.get(url, auth=(TestApi.username, TestApi.password))
        self.assertEqual(200, r.status_code)
        self.assertIsNotNone(r.json())
    
    def test_distance_back_write_read(self):
        url = TestApi.url + "api/v1.0/distance/back"
        value = 3.215
        r = requests.post(url, json={"value": value}, auth=(TestApi.username, TestApi.password))
        self.assertEqual(201, r.status_code)
        r = requests.get(url, auth=(TestApi.username, TestApi.password))
        self.assertEqual(value, r.json()[0]['value'])
    
    def test_colision_get(self):
        url = TestApi.url + "api/v1.0/colision"
        r = requests.get(url, auth=(TestApi.username, TestApi.password))
        self.assertEqual(200, r.status_code)
        self.assertIsNotNone(r.json())
    
    def test_colision_write_read(self):
        url = TestApi.url + "api/v1.0/colision"
        value = 1
        r = requests.post(url, json={"value": value}, auth=(TestApi.username, TestApi.password))
        self.assertEqual(201, r.status_code)
        r = requests.get(url, auth=(TestApi.username, TestApi.password))
        self.assertEqual(value, r.json()[0]['value'])
        value = 2.34
        r = requests.post(url, json={"value": value}, auth=(TestApi.username, TestApi.password))
        self.assertEqual(200, r.status_code)
        r = requests.get(url, auth=(TestApi.username, TestApi.password))
        self.assertEqual(1, r.json()[0]['value'])

    def test_collision_not_valid_data(self):
        url = TestApi.url + "api/v1.0/colision"
        value = 'test'
        r = requests.post(url, json={"value": value}, auth=(TestApi.username, TestApi.password))
        self.assertEqual(500, r.status_code)
        

    def test_specific_collision(self):
        url = TestApi.url + "api/v1.0/colision"
        value = 1
        r = requests.post(url, json={"value": value}, auth=(TestApi.username, TestApi.password))
        self.assertEqual(201, r.status_code)
        r = requests.get(url, auth=(TestApi.username, TestApi.password))
        self.assertEqual(value, r.json()[0]['value'])
        time = r.json()[0]['time'][0:26]
        ts = str(datetime.strptime(time, '%Y-%m-%dT%H:%M:%S.%f').timestamp())
        ts = ts.split(".")
        ts = ts[0]+ts[1][0:3]
        ts = int(ts) + 7200000
        url = TestApi.url + "api/v1.0/colision/" + str(ts)
        r = requests.get(url, auth=(TestApi.username, TestApi.password))
        self.assertEqual(200, r.status_code)

    def test_carmode_get(self):
        url = TestApi.url + "api/v1.0/carmode"
        r = requests.get(url, auth=(TestApi.username, TestApi.password))
        self.assertEqual(200, r.status_code)
        self.assertIsNotNone(r.json())
    
    def test_carmode_write_read(self):
        url = TestApi.url + "api/v1.0/carmode"
        value = 4.565
        r = requests.post(url, json={"value": value}, auth=(TestApi.username, TestApi.password))
        self.assertEqual(201, r.status_code)
        r = requests.get(url, auth=(TestApi.username, TestApi.password))
        self.assertEqual(value, r.json()[0]['value'])

