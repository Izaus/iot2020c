import unittest
import configparser

from backend.database.influx_database import InfluxDatabase

class TestDatabase(unittest.TestCase):

    @classmethod
    def setUpClass(TestDatabase):
        config = configparser.RawConfigParser()
        config.read('backend/configuration.properties')
        TestDatabase.database = InfluxDatabase()
        TestDatabase.database.initializeConnection(
            config.get('DatabaseSection', 'database.address'),
            config.get('DatabaseSection', 'database.port'),
            config.get('DatabaseSection', 'database.username'),
            config.get('DatabaseSection', 'database.password'),
            config.get('DatabaseSection', 'database.name'),
            int(config.get('DatabaseSection', 'database.timeout')),
            int(config.get('DatabaseSection', 'database.retries')))

    def test_connection(self):
        self.assertIsNotNone(TestDatabase
        .database.database_client)

    def test_add_float_value(self):
        value = 1.3
        measurement = 'test_measurement'
        self.assertTrue(TestDatabase.database.insertQuery(measurement, value))
    
    def test_add_text_value(self):
        value = 'test'
        measurement = 'test_measurement'        
        self.assertFalse(TestDatabase.database.insertQuery(measurement, value))
    
    def test_read_value(self):
        value = 2.4
        measurement = 'test_read_measurement'
        limit = '1'
        from_time = 0
        self.assertTrue(TestDatabase.database.insertQuery(measurement, value))
        self.assertEqual(TestDatabase.database.selectQuery(measurement, limit, from_time)[0]['value'],value)
    
    def test_limit_value(self):
        value1 = 2.5
        value2 = 1.2
        measurement = 'test_read_measurement'
        limit = '1'
        from_time = 0
        self.assertTrue(TestDatabase.database.insertQuery(measurement, value1))
        self.assertTrue(TestDatabase.database.insertQuery(measurement, value2))
        self.assertEqual(len(TestDatabase.database.selectQuery(measurement, limit, from_time)),1)
        self.assertEqual(TestDatabase.database.selectQuery(measurement, limit, from_time)[0]['value'],value2)

    @classmethod
    def tearDownClass(TestDatabase):
        TestDatabase.database.database_client.query("DROP MEASUREMENT test_measurement")
        TestDatabase.database.database_client.query("DROP MEASUREMENT test_read_measurement")

if __name__ == '__main__':
    unittest.main()