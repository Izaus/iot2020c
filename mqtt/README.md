### Broker MQTT

Broker Mqtt odpowiada za dystrybucję wiadomości pomięcy instancjami publikującymi je oraz subskrybującymi dany temat. W naszym projekcie rolę tę spełnia otwartoźródłowy program Eclipse Mosquitto (https://mosquitto.org/). Do podstawowego spełniania swojej funkcji Mosquitto nie potrzebuje dodatkowej konfiguracji. Aby zainstalować i urchomić brokera na systemie Ubuntu należy wkonać komendy:

<pre>
apt update && apt upgrade -y
apt install python3-pip mosquitto -y
service mosquitto start
</pre>

W podstawowej konfiguracji komunikacja z brokerem nie jest w żaden sposób zabezpieczana. Aby zwiększyć poziom bezpieczeństwa naszej aplikacji uruchomiliśmy szyfrowanie komunikacji za pomocą __TLS1.3__. W tym celu w należy wygenerować certyfikat CA oraz za jego pomocą podpisać certyfikat poświadczający o tożsamości brokera. Następnie pliki certyfikatów oraz klucza prywatnego brokera należy umieścić w odpowiednich katalogach w `/etc/mosquitto` oraz wprowadzić poniższe zmiany w konfiguracji (`/etc/mosquitto/conf.d/mosquitto.conf`):

<pre>
port 8883

...

cafile /etc/mosquitto/ca_certificates/ca.cert
#capath

# Path to the PEM encoded server certificate.
certfile /etc/mosquitto/certs/mqtt_broker.pem

# Path to the PEM encoded keyfile.
keyfile /etc/mosquitto/certs/mqtt_broker.key
</pre>

### Subskrybent MQTT

Skrypt https://gitlab.com/Izaus/iot2020c/-/blob/dev/mqtt/mqtt_subscriber.py odpowiada za odbieranie danych publikowanych przez inteligentny samochodzik oraz wysyłanie ich do API serwera backend. Napisany został w języku Python.

Do nawiązania połączenia z brokerem MQTT użyta została biblioteka paho-mqtt (https://pypi.org/project/paho-mqtt/). Biblioteka ta pozwala na powołanie obiektu Client, dzięki któremu możliwe jest nawiązywanie połączenia MQTT szyfrowanego TLS. 

Działanie skryptu zaczyna się od wywołania funkcji sleep(5) w https://gitlab.com/Izaus/iot2020c/-/blob/dev/mqtt/mqtt_subscriber.py#L133 . Jest to związane z tym, że skrypt uruchamiany jest w kontenerze i potrzebne było wprowadzenie opóźnienia. Następnie w https://gitlab.com/Izaus/iot2020c/-/blob/dev/mqtt/mqtt_subscriber.py#L135 wywoływana jest funkcja get_api_token(), która została zdefiniowana w https://gitlab.com/Izaus/iot2020c/-/blob/dev/mqtt/mqtt_subscriber.py#L12 . Funkcja wykonuje zapytanie do API serwera backend w celu pobrania tokena, którym następnie uwierzytelniane będą wiadomości z aktualizacjami danych z czujników.

Po pobraniu tokenu powołana zostaje instancja klienta mqtt, do której przekazany zostaje wcześniej pobrany token.
Poprawne nawiązanie bezpiecznej komunikacji przez instancje łączące się do brokera za pośrednictwem paho-mqtt wymaga przesłania na nie również certyfikatu CA oraz przy nawiązywaniu połączenia wskazanie DOKŁADNIE takiej nazwy brokera, na jaką został wystawiony certyfikat:

<pre>
Issuer: CN = CA_SMARTCAR
Validity
    Not Before: Mar 31 15:32:41 2020 GMT
    Not After : Mar 31 15:32:41 2021 GMT
Subject: CN = __mqtt_broker__
</pre>

Certyfikat CA wskazany został w https://gitlab.com/Izaus/iot2020c/-/blob/dev/mqtt/mqtt_subscriber.py#L141
Linia ta jednocześnie wskazuje, że komunikacja powinna zostać zaszyfrowana.

Samo połączenie wywoływane jest w linii https://gitlab.com/Izaus/iot2020c/-/blob/dev/mqtt/mqtt_subscriber.py#144 - jak widać jako pierwszy argument podana została nazwa hosta DOKŁADNIE tak jak jest to zapisane w certyfikacie. W związku z tym, że system operacyjny nie jest w stanie rozwiązać na adres IP za pomocą DNS do /etc/hosts został dodany odpowiedni wpis z adresem IP brokera.


W linii https://gitlab.com/Izaus/iot2020c/-/blob/dev/mqtt/mqtt_subscriber.py#142 domyślna metoda on_connect zostaje nadpisana porzez zdefiniowaną w linii https://gitlab.com/Izaus/iot2020c/-/blob/dev/mqtt/mqtt_subscriber.py#L37 nową funkcję on_connect(). Zmodyfikowana metoda po nawiązaniu połączenia subksrybuje na temat "smartcar/".


W linii https://gitlab.com/Izaus/iot2020c/-/blob/dev/mqtt/mqtt_subscriber.py#L143 metoda on_message definiująca zachowanie klienta po otrzymaniu wiadomości zostaje nadpisana przez zdefiniowaną w linii https://gitlab.com/Izaus/iot2020c/-/blob/dev/mqtt/mqtt_subscriber.py#45 funkcję on_message(). Funkcja odczytuje wiadomość przesłaną protokołem MQTT (https://gitlab.com/Izaus/iot2020c/-/blob/dev/mqtt/mqtt_subscriber.py#53), parsuje ją (https://gitlab.com/Izaus/iot2020c/-/blob/dev/mqtt/mqtt_subscriber.py#L57) i pojedynczo wysyła każdą z zebranych przez czujniki wartości do serwera API. 
<pre>

try:
        api_conn.request("POST", "/api/v1.0/distance/front", body = distance_front_value, headers = hdr)
        response = api_conn.getresponse()
        response.read()
        api_conn.request("GET", "/api/v1.0/distance/front", headers = hdr)
        response = api_conn.getresponse()
        decoded = response.read().decode()
        results.append("distance front: " + str(decoded[0]["value"]))
    
    except:
        print("Failed sending front distance data")

</pre>

Dane odebrane od brokera są wypisywane na ekran, a po każdorazowym wysłaniu wiadomości wykonane jest również zapytanie do API celem odczytania aktualnej wartości, które po wysłaniu wszystkich wartości wyświetlane są na ekran. Dzięki temu możliwe jest porównanie czy informacje w bazie danych zostały poprawnie zaktalizowane.

### Lessons learned

Na początku projektu podjęliśmy decyzję o wykorzystaniu protokołu LWM2M do przesyłania informacji (https://omaspecworks.org/what-is-oma-specworks/iot/lightweight-m2m-lwm2m/). Decyzję tę podjęliśmy ze względu liczne możliwości tego protokołu. Niestety przy próbie implementacji okazało się, że skorzystanie z dostępnych bibliotek i narzędzi jest skomplikowane oraz wykracza poza nasze umiejętności korzystania z języków programowania, w których zostały zaimplementowane. Dodatkowo reszta zespołu zaczęłą implementować swoje części projektu w języku Python, do którego nie udało nam się znaleźć odpowiednich bibliotek. Z tego powodu zmieniliśmy koncepcję i wybraliśmy protokół MQTT, jako broker wiadomoci pakiet Mosquitto (https://mosquitto.org/), a jako bibliotekę do komunikacji (https://pypi.org/project/paho-mqtt/)



