import paho.mqtt.client as mqtt
from os import environ
import http.client
import ssl
import json
from base64 import b64encode
import getpass
from time import sleep



def get_api_token():
    
    try:
        user = environ['API_USER']
        password = environ['API_PASSWORD']
        server = environ['API_SERVER']
    except:
        print("Error with API variables")
        return 1
    
    
    uap = user + ":" + password
    headers = {"Authorization" : "Basic %s" % b64encode(bytes(uap, encoding="utf-8")).decode("ascii") }
    
    try:
        connection = http.client.HTTPSConnection(server, 443, context=ssl._create_unverified_context())
        connection.request("GET", "/api/v1.0/token", headers = headers)
        response = connection.getresponse()
        decoded_response = json.loads(response.read().decode())
        connection.close()
        return decoded_response["token"]
    except:
        print("Error with retrieving API token")
        return 1

def on_connect(client, userdata, flags, rc):
    try:
        client.subscribe("smartcar/")
        print("Connected to MQTT broker with result code "+str(rc))
    except:
        print("Error with subscribing to topic")
        return 1

def on_message(client, userdata, msg):
    
    server = environ["API_SERVER"]
    auth_string = userdata + ":0"

    hdr = {"Authorization" : "Basic %s" % b64encode(bytes(auth_string, encoding="utf-8")).decode("ascii") ,
            "Content-type" : "application/json"}
    
    readings = msg.payload
    data = json.loads(readings.decode())
    print("Data sent from smartcar: " + readings.decode())
    
    try:
        velocity_value = json.dumps({"value": data["velocity"]})
        distance_front_value = json.dumps({"value": data["distance_front"]})
        distance_back_value = json.dumps({"value": data["distance_back"]})
        carmode_value = json.dumps({"value": data["carmode"]})
        collision_value = json.dumps({"value": data["collision"]})
    except:
        print("Error parsing data")
        return 1
    
    api_conn = http.client.HTTPSConnection(server, 443, context=ssl._create_unverified_context())
    
    try:
        api_conn.request("POST", "/api/v1.0/distance/front", body = distance_front_value, headers = hdr)
        response = api_conn.getresponse()
        response.read()
        api_conn.request("GET", "/api/v1.0/distance/front", headers = hdr)
        response = api_conn.getresponse()
        print("Current reading of distance front: " + response.read().decode())
    
    except:
        print("Failed sending front distance data")

    try:
        api_conn.request("POST", "/api/v1.0/distance/back", body = distance_back_value, headers = hdr)
        response = api_conn.getresponse()
        response.read()
        api_conn.request("GET", "/api/v1.0/distance/back", headers = hdr)
        response = api_conn.getresponse()
        print("Current reading of distance back data: " + response.read().decode())
    except:
        print("Failed sending back distance data")


    try:
        api_conn.request("POST", "/api/v1.0/carmode", body = carmode_value, headers = hdr)
        response = api_conn.getresponse()
        response.read()
        api_conn.request("GET", "/api/v1.0/carmode", headers = hdr)
        response = api_conn.getresponse()
        print("Current reading of carmode data: " + response.read().decode())
    except:
        print("Failed sending carmode data")


    try:
        api_conn.request("POST", "/api/v1.0/velocity", body = velocity_value, headers = hdr)
        response = api_conn.getresponse()
        response.read()
        api_conn.request("GET", "/api/v1.0/velocity", headers = hdr)
        response = api_conn.getresponse()
        print("Current reading of velocity data: " + response.read().decode())
    except:
        print("Failed sending velocity data")


    try:
        api_conn.request("POST", "/api/v1.0/colision", body = velocity_value, headers = hdr)
        response = api_conn.getresponse()
        response.read()
        api_conn.request("GET", "/api/v1.0/colision", headers = hdr)
        response = api_conn.getresponse()
        print("Current reading of collision data: " + response.read().decode())
    except:
        print("Failed sending collision data")

    api_conn.request("GET", "/api/v1.0/distance/front", headers = hdr)
    response = api_conn.getresponse()
    print("Aktaulny odczyt: " + response.read().decode())
    
    api_conn.close()

sleep(5)

token = get_api_token()

if token == 1:
    print("Due to errors could not proceed.. going to exit")
else:
    client = mqtt.Client(userdata = token)
    client.tls_set('./ca.cert')
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect("mqtt_broker", 8883, 60)
    client.loop_forever()
