# Start application:
To start application, first make sure that SSL cert files are in correct directory:
- `./nginx/certs/cert.crt`
- `./nginx/certs/cert.key`

To start up project run:
`docker-compose up -d`

Projekt składa się z kilku kontenerów:
- backend + frontend - flask (Docker: flaskapp)
- baza danych - influxdb (Docker: influxdb)
- mqtt broker + subscriper - Mosquitto + Python (Docker: mqtt)
- Reverse proxy - nginx (Docker: rev_proxy)


`rev_proxy` container acts as SSL reverse proxy which encrypts traffic to website (+ web sockets and REST API)

![](./images/SmartCar_diagram_v0.3.png)



---
[Demo hosted on youtube.com](https://youtu.be/OHlO9SVGh5c)
