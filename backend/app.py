"""Python Flask IoT Server
Created on: 21.03.2020
@author: Radoslaw Jajko (radek2s)

This script is responsible for handle incoming messages from different sources
to save data to database and to send to connected browser clients.

@version: 1.0.0
@updated: 20.04.2020
"""
import configparser, os
from database.influx_database import InfluxDatabase
from guard.validation import Validation
from flask import Flask, render_template, jsonify, request, g, redirect, url_for, make_response
from flask_socketio import SocketIO, emit
from flask_httpauth import HTTPBasicAuth

config = configparser.RawConfigParser()
config.read('configuration.properties')

application = Flask(__name__, static_url_path='', static_folder='./frontend', template_folder='./frontend/templates')
application.config['SECRET_KEY']= 'IoTsecret'
auth = HTTPBasicAuth()
database = InfluxDatabase()
validation = Validation(application.config['SECRET_KEY'])
socket_io = SocketIO(application, cors_allowed_origins="*")
verbose = config.getboolean('ServerSection', 'server.verbose')

def run():
    """
        Initialization of the backend application. Requires a configuartion.properties file to start.
    """

    db_connected = database.initializeConnection(
        config.get('DatabaseSection', 'database.address'),
        config.get('DatabaseSection', 'database.port'),
        config.get('DatabaseSection', 'database.username'),
        config.get('DatabaseSection', 'database.password'),
        config.get('DatabaseSection', 'database.name'),
        int(config.get('DatabaseSection', 'database.timeout')),
        int(config.get('DatabaseSection', 'database.retries')))

    if db_connected:
        host = config.get('ServerSection', 'server.address')
        port = config.get('ServerSection', 'server.port')
        print("Starting Flask server on {}:{}".format(host, port))
        validation.add_user(
            config.get('UserSection', 'user.name'), 
            config.get('UserSection', 'user.pass'))
        
        if verbose:
            print("  **************************")
            print("  *  Verbose mode enabled  *")
            print("  **************************")
        
        if config.getboolean('ServerSection', 'server.production'):
            print("    Running in production mode...")
            cer = os.path.dirname(os.path.realpath(__file__))+"/cert.pem"
            key = os.path.dirname(os.path.realpath(__file__))+"/key.pem"
            socket_io.run(application, host=host, port=port, keyfile=key, certfile=cer)

        else:
            print("    Running in developement mode...")
            socket_io.run(application, host=host, port=port)

    else:
        print("Stopping Flask server...")

@application.route('/')
def index():
    """
    Flask application main webpage. Validate user session and if it is valid 
    allow to display the index.html page. If user in not logged in redirect them
    to login_page.
    """
    if 'session' in request.cookies:
        user = validation.verify_auth_token(request.cookies.get('session'))
        if not user:
            return redirect(url_for('login_page'))
        if 'username' in request.cookies:
            return render_template('index.html', 
            applicationUser=request.cookies.get('username'),
            socketProtocol=config.get('WebSection', 'websocket.protocol'),
            socketAddress=config.get('WebSection', 'websocket.address'),
            socketPort=config.get('WebSection', 'websocket.port'),
            socketName=config.get('WebSection', 'websocket.name'))
        else:
            return redirect(url_for('login_page'))
    else:
        return redirect(url_for('login_page'))
    

@application.route('/login', methods=['GET', 'POST'])
def login_page():
    """
    Flask application login page. If this method receive a POST request with valid
    user credentials, generate a token and redirect user to main page.
    """
    try:
        user = validation.get_user(request.form['username'], request.form['password'])
        if not user:
            return render_template('login.html')
        else:
            response = make_response(redirect('/'))
            response.set_cookie('session', validation.generate_auth_token(user.id))
            response.set_cookie('username', user.username)
            return response
    except Exception:

        return render_template('login.html')
    

@application.route('/logout')
def logout_page():
    """
    Clear user cookies and display logout page.
    """
    response = make_response(render_template('logout.html'))
    response.set_cookie('session', '', expires=0)
    response.set_cookie('username', '', expires=0)
    return response

@auth.verify_password
def verify_password(username_or_token, password):
    user = validation.verify_auth_token(username_or_token)
    if not user:
        user = validation.get_user(username_or_token, password)
        if not user:
            return False
    g.user = user
    return True

@application.route('/api/v1.0/token')
@auth.login_required
def get_auth_token():
    token = validation.generate_auth_token(g.user.id)
    return jsonify({'token': token.decode('ascii')})

# ---- Car colision methods ----
@application.route('/api/v1.0/colision', methods=['GET'])
@auth.login_required
def get_car_colisions():
    """
    Flask api GET method which allows to make a request for collision data.
    Request arguments:
        length - Length of the result array. Limit the response values
        from   - Time from last X minutes
    Result: 
        array  - array of collisions with timestamps
    """
    if verbose:
        print("Received a new request for {}".format("Colision list"))
    result_limit = request.args.get('length', default = 1, type = int)
    end_timestamp = request.args.get('from', default = 0, type = int)
    return jsonify(database.selectQuery('colision', str(result_limit), end_timestamp))

@application.route('/api/v1.0/colision/<timestamp>', methods=['GET'])
@auth.login_required
def get_car_colision(timestamp):
    """
    Flask api GET method which allows to make a request for a specific collision data.
    Request arguments:
        length - Length of the result array. Limit the response values
        from   - Time from last X minutes
    Result: 
        array  - array of objects = {distance, time, type}
    """
    if verbose:
        print("Received a new request for {}".format("Collision"))
    result_limit = request.args.get('length', default = 120, type = int)
    end_timestamp = request.args.get('from', default = 1, type = int)
    return jsonify(database.selectColision(timestamp, end_timestamp, result_limit))

@application.route('/api/v1.0/colision', methods=['POST'])
@auth.login_required
def set_car_colision():
    """
    Flask api POST method which add a new value to colision measurement in database.
    If measurement has been written correctly return HTTP code 201 with json status message 'done'. 
    When database error occured response message is 'error'. 
    When user do not send a vaild request response message is 'failed',.
    Result: 
        json message {'status':['error'|'failed'|'done'|'not_saved'|'Parsing exception']}
        HTTP response code 400, 200, 201, 500
    """
    if not request.json or not 'value' in request.json:
        return jsonify({'status': 'failed'}), 400
    try:
        if int(request.json['value']) == 1:
            if database.insertQuery('colision', request.json['value']):
                if verbose:
                    print("Received a new value for {}:{}".format("Collision", request.json['value']))
                socket_io.emit('Update colision', request.json['value'], namespace='/socket')
                return jsonify({'status': 'done'}), 201
            else:
                return jsonify({'status': 'error'}), 400
        else:
            return jsonify({'status': 'not_saved'}), 200
    except:
        return jsonify({'status':'Parsing exception'}), 500
        print("[Exception] Not valid data")
    
    

# ---- Car mode methods ----
@application.route('/api/v1.0/carmode', methods=['GET'])
@auth.login_required
def get_car_mode():
    """
    Flask api GET method which allows to make a request for a car mode data.
    Result: 
        array  - array with carmode value with timestamp
    """
    if verbose:
        print("Received a new request for {}".format("Carmode"))
    return jsonify(database.selectQuery('carmode', '1', 0))


@application.route('/api/v1.0/carmode', methods=['POST'])
@auth.login_required
def set_car_mode():
    """
    Flask api POST method which add a new value to carmode measurement in database.
    If measurement has been written correctly return HTTP code 201 with json status message 'done'. 
    When database error occured response message is 'error'. 
    When user do not send a vaild request response message is 'failed',.
    Result: 
        json message {'status':['error'|'failed'|'done']}
        HTTP response code 400 or 201
    """
    if not request.json or not 'value' in request.json:
        return jsonify({'status': 'failed'}), 400
    if database.insertQuery('carmode', request.json['value']):
        if verbose:
            print("Received a new value for {}:{}".format("Carmode", request.json['value']))
        socket_io.emit('Update mode', request.json['value'], namespace='/socket')
        return jsonify({'status': 'done'}), 201
    else:
        return jsonify({'status': 'error'}), 400

# ---- Car front distance methods ----
@application.route('/api/v1.0/distance/<direction>', methods=['GET'])
@auth.login_required
def get_car_distance(direction):
    """
    Flask api GET method which allows to make a request for a specific distance data.
    Arguments:
        direction - [front|back] - type of the direction.
    Request arguments:
        length - Length of the result array. Limit the response values
        from   - Time from last X minutes
    Result: 
        array  - array of distance values with timestamps
    """
    if verbose:
        print("Received a new request for direction:{}".format(direction))
    result_limit = request.args.get('length', default = '1', type = str)
    end_timestamp = request.args.get('from', default = 0, type = int)
    return jsonify(database.selectAdvancedQuery('distance', result_limit, direction, end_timestamp))

@application.route('/api/v1.0/distance/<direction>', methods=['POST'])
@auth.login_required
def set_car_distance_back(direction):
    """
    Flask api POST method which add a new value to distance measurement in database.
    If measurement has been written correctly return HTTP code 201 with json status message 'done'. 
    When database error occured response message is 'error'. 
    When user do not send a vaild request response message is 'failed',.
    Arguments:
        direction - [front|back] - type of the direction
    Result: 
        json message {'status':['error'|'failed'|'done']}
        HTTP response code 400 or 201
    """
    if not request.json or not 'value' in request.json:
        return jsonify({'status': 'failed'}), 400
    if database.insertAdvancedQuery('distance', request.json['value'], direction):
        if verbose:
            print("Received a new value for direction {}:{}".format(direction, request.json['value']))
        if direction == 'front':
            socket_io.emit('Update distance front', request.json['value'], namespace='/socket')
        if direction == 'back':
            socket_io.emit('Update distance back', request.json['value'], namespace='/socket')
        return jsonify({'status': 'done'}), 201
    else:
        return jsonify({'status': 'error'}), 400

# ---- Car velocity methods ----
@application.route('/api/v1.0/velocity', methods=['GET'])
@auth.login_required
def get_car_velocity():
    """
    Flask api GET method which allows to make a request for a specific velocity data.
    Request arguments:
        length - Length of the result array. Limit the response values
        from   - Time from last X minutes
    Result: 
        array  - array of velocity with timestamps
    """
    if verbose:
        print("Received a new request for {}".format("Velocity"))
    result_limit = request.args.get('length', default = 1, type = int)
    end_timestamp = request.args.get('from', default = 0, type = int)
    return jsonify(database.selectQuery('velocity', str(result_limit), end_timestamp))

@application.route('/api/v1.0/velocity', methods=['POST'])
@auth.login_required
def set_car_velocity():
    """
    Flask api POST method which add a new value to velocity measurement in database.
    If measurement has been written correctly return HTTP code 201 with json status message 'done'. 
    When database error occured response message is 'error'. 
    When user do not send a vaild request response message is 'failed',.
    Result: 
        json message {'status':['error'|'failed'|'done']}
        HTTP response code 400 or 201
    """
    if not request.json or not 'value' in request.json:
        return jsonify({'status': 'failed'}), 400
    if database.insertQuery('velocity', request.json['value']):
        if verbose:
            print("Received a new value for {}:{}".format("Velocity", request.json['value']))
        socket_io.emit('Update velocity', request.json['value'], namespace='/socket')
        return jsonify({'status': 'done'}), 201
    else:
        return jsonify({'status': 'error'}), 400

# -----------------------------------------------------#
# ___________________ SOCKET_IO _______________________#

@socket_io.on('connect', namespace=config.get('WebSection', 'websocket.name'))
def socket_connect():
    emit('my response', 'Connected')

if __name__ == "__main__":
    run()
