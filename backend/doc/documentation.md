# Projekt SmartCar
Dokumentacja techniczna serwera aplikacji, wykorzystanych środowisk, frameworków oraz komunikacji z bazą danych.
## Aplikacja serwerowa Flask
Do realizacji projektu został wykorzystany Python w wersji 3.8 wraz z wykorzystanym [fameworkiem Flask (v.1.1)](https://flask.palletsprojects.com/en/1.1.x/). Głównym zadaniem aplikacji serwerowej było zapewnienie komunikacji z poszczególnymi komponentami systemu. Przyjmowanie danych z urządzeń RaspberyPI, przetwarzanie danych, zapisywanie ich do bazy danych i możliwość komunikacji z interfejstem użytkownika poprzez API, lub WebSockety. 

### Dlaczego Flask?
Framework Flask został wybrany ponieważ jest on dość lekkim i prostym środowiskiem, opartym o język Python pozwalający na odbieranie i wysyłanie danych poprzez zapytania REST. Wstępnie rozważany był również oparty Java [Spring Boot](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/), który zapewniał kompeksowe rozwiązania. Jednakże chceliśmy mieć spójne środowisko, a na urządzeniach RaspberyPI aplikacje musiały być napisane w Pythone, więc podjęliśmy decyzję, żeby ostatecznie skorzystać z mini-frameworku Flask. Flask wymaga dużo mniejszej ilości kodu do zapewnienia funkcjonalności i jest łatwiej w nim zrobić aplikację na małą skalę. Spring Boot świetnie sprawdziłby się w aplikacji rozwijanej długoterminowo z większą liczbą deweloperów.

### Zależności
Serwer Flask wykorzystuje następujące zależności:
- influxdb - [dokumentacja zewnętrzna](https://github.com/influxdata/influxdb-python)
- Flask-SocketIO - [dokumentacja zewnętrzna](https://flask-socketio.readthedocs.io/en/latest/)
- Flask-httpauth - [dokumentacja zewnętrzna](https://flask-httpauth.readthedocs.io/en/latest/)

### Dokładny opis metod:
Szczegółowy opis metod można znaleźć w dokumentacji modułu znajdujący się w katalogu [/backend/doc/index.html](./index.html). Można tam znaleźć dokładny opis przyjmowanych metod, realizowanych funkcji oraz zwracanych wartości. 

### Inne
 Aplikacja uruchamiana jest w oparciu o plik [configuration.properties](../configuration.properties), w którym zapisane są wszystkie możliwe zmienne do konfiguracji serwera. 

## Moduł influxdb  
Odpowiada on za komunikację serwera z bazą danych. W katalogu /backend/database można znaleźć klasę odpoweidzalną za połaczenie z bazą InfluxDB. Klasa ta pozwala utworzyć obiekt pozwalający na wymianę danych z bazą. Wymagane jest zainicjalizowanie połączenia podając lokalizację serwera bazodanowego i dane do uwierzytelnienia użytkownika. Metoda initializeConnection wewnątrz klasy InfluxDatabase. W przypadku braku połączenia zwracana jest wartość False dzięki której można przerwć działanie serwera.

Klasa InfluxDatabase zawiera również metody do wysyłania zapytań do bazy danych. Przygotowane zapytania są przekazywane według metody `InfluxDBClient().query(...)`. Pobieranie wartości poszczególnych pomiarów realizowane jest poprzez metodę `.get_points(measurement=<nazwa_pomiaru>)` Dzięki temu zwracana jest lista z poszczególnymi wartościami w czasie.

Dokładny opis działania klasy InfluxDatabase można znaleźć w [dokumentacji projektu](./influx_database.html). Zawarte w niej są wszelkie techniczne parametry, wypisane metody, przyjmowane i zwracane argumenty. 

### Dlaczego influxDB?
Potrzebna była baza, która będzie sobie radzić z wieloma zapisami i odczytami danych w czasie rzeczywistym. Najlepszymi narzędziami do rozwiązania tego problemu są bazy danych oparte o czasowe serie danych (time-series databses). Wykorzystaliśmy InfluxDB, ponieważ jest projektem open-source, który ma niski próg wejścia i jest bardzo podobny do klasycznych relacyjnych baz danych poprzez stosowanie InfluxQL (Influx Query Language), który jest bardzo zbliżony do języka SQL. Rozważany był również [Timescale DB](https://docs.timescale.com/latest/tutorials/tutorial-hello-timescale), ale z powodu większej złożoności, mniejszej elastyczności tworzenia rekordów oraz ograniczonej liczby gotowych bibliotek do wysyłania zapytań zdecydowaliśmy, aby skorzystać z [InfluxDB](https://docs.influxdata.com/influxdb/v1.8/). 

## Moduł WebSocket
WebSockety zapewniają komunikajcę między aplikacją serwera a przeglądarką użytkownika dwukierunkową komunikację. W projekcie SmartCar w momencie otrzymywania nowego wpisu do bazy danych odpowiedni komunikat jest wysyłany do wszystkich podłączonych klientów. Przykładowy snippet kodu wykorzystywany w aplikacji app.py zaprezentowany został poniżej. 
``` python
def set_car_colision():
    ...
        socket_io.emit('Update colision', request.json['value'], namespace='/socket')
    ...
```
Na temat "Update colision" wysyłana jest wartość [value] otrzymana w zapytaniu POST.  
Zapewniona jest dwukierunkowa komunikacja, ale klient do serwera wysyła jedynie wiadomość o jego podłączeniu. 
``` python
@socket_io.on('connect', namespace=config.get('WebSection', 'websocket.name'))
def socket_connect():
    emit('my response', 'Connected')

```

## Moduł Bezpieczeństwa
Moduł backent/guard odpowiada za uwierzytelnianie użytkowników zarządzanie nimi. Dzięki stosowaniu uwierzytelnienia basic HTTP authoriztaion interfej API aplikacji Flask został zabezpieczony przez nieautoryzowanym dostępem. W ramach usprawnienia wysyłanych żądań możliwe jest uwierzytelnienie za pomocą tokena. 

Dokładny opis poszczególnych metod jest zawarty w _/backend/doc/validation.html_ oraz _/backend/doc/user.html_. 