# IoT Server
- @version: 0.0.2
- @author: [Radek Jajko](mailto:rjajko@student.agh.edu.pl)

Flask python server to handle incoming traffic with data from SmartCar. 
Server use InfluxDB database to ensure high performance with published and presented data.

## How to start:
Make sure you have installed InfluxDB 1.7.X service and it is running otherwise server won't start.

To start server use command: `python3 app.py`

## HTTP Web-API:

__IMPORTANT:__  
Every API endpoint is protected and require a valid user token or knowledge about username and password to be logged in! 

Interface to communicate with a python server for adding a new measurements or to get them from the database:
- Token [GET]:
    - Resource: `../api/v1.0/token`
    - Response: if OK (200): `{}` type: `JSON Object`
    example:  
    `{"token":"TOKEN_DATA"}`

- Colision [GET]:
    - Resource:
    `../api/v1.0/colision`
    - Request paramters: `length` type: `int` default `1` example:  
    `../api/v1.0/colision?length=10`
    - Request paramters: `from` type: `int` default `5` (in minutes) example:  
    `../api/v1.0/colision?from=40`  
    `../api/v1.0/colision?from=30&length=5`
    - Response: if OK (200): `[]` type: `array of objects with distance from specific period`   

- Colision/ID [GET]:
    - Resource:
    `../api/v1.0/colision/{timestamp}`
    - Request paramters: `from` type: `int` default `5` (in minutes) 
    example:  
    `../api/v1.0/colision/1586897209468?from=10`
    - Response: if OK (200): `[]` type: `array of objects`

- Colision [SET]:
    - Resource:
    `../api/v1.0/colision`
    - RequestBody: `{"value":10}` JSON with "value" parameter [type: float]
    - Response: if OK (201): `{"status":"done"}`

- Car Mode [GET]:
    - Resource:
    `../api/v1.0/carmode`
    - Response: if OK (200): `[]` type: `array of objects` example:      
    `[{"time":"2020-03-21T22:37:27.490256246Z", "value":10.0}]`
- Car Mode [SET]:
    - Resource:
    `../api/v1.0/carmode`
    - RequestBody: `{"value":1.0}` JSON with "value" parameter [type: double]
    - Response: if OK (201): `{"status":"done"}`

- Distance [GET]:
    - Resource:
    `../api/v1.0/distance/{direction}`
    - URI Parameters: `direction` type: `string` acceptable value: `[front|back]`
    - Request paramters: `length` type: `int` default `1` example:  
    `../api/v1.0/distance/{direction}?length=10`
    - Request paramters: `from` type: `int` default `5` (in minutes) example:  
    `../api/v1.0/distance/{direction}?from=40`  
    `../api/v1.0/distance/{direction}?from=30&length=5`
    - Response: if OK (200): `[]` type: `array of objects` example:      
    `[{"time":"2020-03-21T22:37:27.490256246Z", "value":10.0}]`
- Distance [SET]:
    - Resource:
    `../api/v1.0/distance/{direction}`
    - URI Parameters: `direction` type: `string` acceptable value: `[front|back]`
    - RequestBody: `{"value":10.0}` JSON with "value" parameter [type: float]
    - Response: if OK (201): `{"status":"done"}`

- Velocity [GET]:
    - Resource:
    `../api/v1.0/velocity`
    - Request paramters: `length` type: `int` default `1` example:  
    `../api/v1.0/velocity?length=10`
    - Request paramters: `from` type: `int` default `5` (in minutes) example:  
    `../api/v1.0/velocity?from=40`  
    `../api/v1.0/velocity?from=30&length=5`
    - Response: if OK (200): `[]` type: `array of objects` example:      
    
- Velocity [SET]:
    - Resource:
    `../api/v1.0/velocity/{direction}`
    - RequestBody: `{"value":10.0}` JSON with "value" parameter [type: float]
    - Response: if OK (201): `{"status":"done"}`

### How to use?

Firstly you have to login into the application and receive a token:
```shell
curl -u <<username>>:<<password>> http://localhost:8000/api/v1.0/token
```
_Token is valid for about 10 minutes as default._

Receiving a specific resource:
```
curl -u <<_TOKEN_>>:0 http://localhost:8000/api/v1.0/carmode
```
Sending a specific resource:
```
curl -u <<_TOKEN_>>:0 -i -H "Content-Type: application/json" -X POST -d '{"value":13.5}' https://localhost:8000/api/v1.0/velocity -k
```
## WebSocket Web-API:
WebSocket listen on namesapce "`/socket`" for specified topics:
- `Update mode`  
  when a new `carmode` measurement is received
- `Update distance front`  
  when a new `distance/front` measurement is received
- `Update distance back`  
when a new `distance/back` measurement is received
- `Update velocity`  
when a new `velocity` measurement is received
- `Update colision`  
when a new `colision` measurement is received


On update this socket returns a double value of last received measurement. Each event is performed when specific POST method is invoked.

### How to use?

```HTML
<script>
var socket = io.connect('<<ADDRESS_AND_PORT>>/socket', {secure: true, rejectUnauthorized : false})

socket.on('connect', function () {
    socket.emit('my event', { data: 'I\'m connected!' });
});

socket.on('Update distance front', function (distance) {
    console.log(distance) //Display Front Distance
});
...
</script>
```

## Docker:
Building image (when in directiory _/server/serveriot/_):
- `docker-compose build`

Running containers with Flask and InfluxDB:
- `docker-compose up -d`

Shutdown the containers:
- `docker-compose down`

# Documentation:

Documentation of the python code files could be found [here](./doc/index.html)