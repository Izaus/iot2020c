"""Python InfluxDB connector
Created on: 21.03.2020
@author: Radoslaw Jajko (radek2s)

Python script that create a InfluxDatabase class to establish a connection
with InfluxDB server. 

@version: 1.0.0
@updated: 20.04.2020
"""

from influxdb import InfluxDBClient
import time

class InfluxDatabase:
    """
        InfluxDatabase class to control and manage database
    """

    def initializeConnection(self, host, port, user, password, database, timeout, retries):
        print("Connecting with influxDB server...")
        print("    {}:{}/{} as '{}' user.".format(host, port, database, user))
        self.database_client = InfluxDBClient(host, port, user, password, database)
        for i in range(retries):
            try:
                self.database_client.get_list_database()
                print("    Database connection established!")
                return True
            except Exception:
                if i == retries-1:
                    print("    Database connection failed!")
                    return False
                print("    ... trying to connect ... #{}".format(i))
                time.sleep(timeout)

    
    def selectQuery(self, measurements, limit, from_time):
        """Select data from specific measurement from database .
        
        Args:
            measurements (string): the table name
            limit (string):        number of returned values
            from_time (int):       last X minutes of measurements

        Returns:
            list: Contains measurement point values with timestamp 
        """

        if from_time == 0:
            query = "SELECT value FROM " + measurements + " ORDER BY time DESC LIMIT " + limit
        else:
            if limit == '1':
                query = "SELECT value FROM " + measurements + " WHERE time > now()-" + str(from_time) + "m ORDER BY time DESC"
            else:
                query = "SELECT value FROM " + measurements + " WHERE time > now()-" + str(from_time) + "m ORDER BY time DESC LIMIT " + limit
        return list(self.database_client.query(query).get_points(measurement=measurements))

    def selectAdvancedQuery(self, measurements, limit, type, from_time):
        """Select data from distance measurement from database where type is required.
        
        Args:
            measurements (string): the table name
            limit (string):        number of returned values
            type (string):         [front|back] acceptable values
            from_time (int):       last X minutes of measurements

        Returns:
            list: Contains measurement point values with timestamp 
        """
        if from_time == 0:
            query = "SELECT value FROM " + measurements + " WHERE type='" + type + "'ORDER BY time DESC LIMIT " + limit
        else:
            if limit == '1':
                query = "SELECT value FROM " + measurements + " WHERE type='" + type + "' AND time > now()-" + str(from_time) + "m ORDER BY time DESC"
            else:
                query = "SELECT value FROM " + measurements + " WHERE type='" + type + "' AND time > now()-" + str(from_time) + "m ORDER BY time DESC LIMIT " + limit
        return list(self.database_client.query(query).get_points(measurement=measurements))

    def selectColision(self, timestamp, end_timestamp, limit):
        """Select data from distance measurement to show colision history
        
        Args:
            timestamp (string):  Id of colision
            end_timestamp (int): Number of minutes of data window
            limit (int):         Number of samples returned from database

        Returns:
            list: Contains distance measurement point values with timestamp.
        """
        timestamp = int(timestamp) + (1000 * 10)
        endtimestamp = timestamp - (end_timestamp*1000*60)
        if limit == 120:
            query = "SELECT value, type FROM distance WHERE (time <= "+ str(timestamp)+"000000 AND time >= "+str(endtimestamp)+"000000) ORDER BY time DESC"
        else:
            query = "SELECT value, type FROM distance WHERE (time <= "+ str(timestamp)+"000000 AND time >= "+str(endtimestamp)+"000000) ORDER BY time DESC LIMIT " + str(limit)
        return list(self.database_client.query(query).get_points(measurement='distance'))

    def insertQuery(self, measurement, value):
        """Insert a new data to specific measurement.
        
        Args:
            measurements (string): the table name
            value (float):         a new value to save
        Returns:
            bool: True if value has been added corectly, false otherwise
        """
        try:
            json_body = [{
                "measurement": measurement,
                "fields": {
                    "value": float(value)
                }
            }]
            return self.database_client.write_points(json_body)
        except ValueError:
            return False
        

    def insertAdvancedQuery(self, measurement, value, type):
        """Insert a new data to specific measurement.
        
        Args:
            measurements (string): the table name
            value (float):         a new value to save
            type (string):         [front|back] type of measurement
        Returns:
            bool: True if value has been added corectly, false otherwise
        """
        try:
            json_body = [{
                "measurement": measurement,
                "tags": {
                    "type": type,
                },
                "fields": {
                    "value": float(value)
                }
            }]
            return self.database_client.write_points(json_body)
        except ValueError:
            return False
