"""Validation script
Created on: 21.03.2020
@author: Radoslaw Jajko (radek2s)

User class

@version: 1.0.0
@updated: 20.04.2020
"""
from passlib.apps import custom_app_context as pwd_context

class User:
    """
    SmartCar user class. Each user has unique ID, name and encrypted password.
    """

    max_id = 0

    def __init__(self, username, password):
        self.id = User.max_id
        User.max_id += 1 
        self.username = username
        self.password = self.hash_password(password)
    
    def hash_password(self, password):
        return pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password)

    