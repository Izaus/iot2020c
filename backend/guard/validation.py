"""Validation script
Created on: 21.03.2020
@author: Radoslaw Jajko (radek2s)

This script is responsible for security in IoT SmartCar application.

@version: 1.0.0
@updated: 20.04.2020
"""
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
from .user import User

class Validation():
    """
    Validation class to handle users and them credentials. Provides an authentication mechanism.
    """

    def __init__(self, secret):
        self.secret = secret
        self.users = []

    def add_user(self, username, password):
        """Create a new user for the application.
        
        Args:
            username (string): name of the user
            password (string): user's password
        """
        self.users.append(User(username,password))

    def get_user(self, username, password):
        """Return a user from the application database.
        
        Args:
            username (string): name of the user
            password (string): user's password

        Returns:
            user: Valid user object. 
        """
        for user in self.users:
            if user.username == username:
                if user.verify_password(password):
                    return user
        return None

    def generate_auth_token(self, user_id, expiration = 2592000):
        """Create a new user token to access to the application.
        
        Args:
            userid (string): user identifier
            expiration (int): token valid time in seconds

        Returns:
            json {id: token} - where token in an unique random sequence
        """
        s = Serializer(self.secret, expires_in = expiration)
        return s.dumps({'id': user_id})
    
    def verify_auth_token(self, token):
        """Validate token
        
        Args:
            token (string): user's token data

        Returns:
            user - valid user object if token was correct.
        """
        s = Serializer(self.secret)
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None
        except BadSignature:
            return None
        user = self.users[data['id']]
        return user
